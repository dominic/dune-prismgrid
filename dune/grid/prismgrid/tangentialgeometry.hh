#ifndef DUNE_GRID_PRISMGRID_TANGENTIALGEOMETRY_HH
#define DUNE_GRID_PRISMGRID_TANGENTIALGEOMETRY_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/grid/common/geometry.hh>

namespace Dune
{

  // PrismJacobianHelper 
  // -------------------

  template< class ct, int mydim, int cdim >
  class PrismJacobianHelper
  {
    typedef ct ctype;

    static const int mydimension = mydim;
    static const int coorddimension = cdim;

    typedef FieldMatrix< ctype, cdim, mydim > JacobianInverseTransposed;

    PrismJacobianHelper ()
    {
      for( int i = 0; i < cdim; ++i )
        for( int j = 0; j < mydim; ++j )
          jacobian_[ i ][ j ] = ( i == j ) ? 1 : 0;
    }

  public:
    static const JacobianInverseTransposed &instance ()
    {
      static PrismJacobianHelper instance_;
      return instance_.jacobian_;
    }

  private:
    JacobianInverseTransposed jacobian_;
  };



  // PrismLocalTangentialGeometry
  // ----------------------------

  template< class HostGeometry >
  struct PrismLocalTangentialGeometry
  {
    static const int mydimension = HostGeometry::mydimension;
    static const int coorddimension = HostGeometry::dimension;
    static const int dimension = HostGeometry::dimension;

    typedef typename HostGeometry::ctype ctype;

    typedef FieldVector< ctype, mydimension > LocalCoordinate;
    typedef FieldVector< ctype, coorddimension > GlobalCoordinate;

    typedef FieldMatrix< ctype, coorddimension, mydimension > JacobianInverseTransposed;
    typedef FieldMatrix< ctype, mydimension, coorddimension > JacobianTransposed;

  public:
    PrismLocalTangentialGeometry ( const HostGeometry& hostGeometry )
    : hostGeometry_( hostGeometry ),
      hostReferenceElement_( ReferenceElements< ctype, dimension >::general( type() ) )
    {}

    GeometryType type () const { return hostGeometry().type(); }

    bool affine () const { return hostGeometry().affine(); }

    int corners () const { return hostGeometry().corners(); }

    GlobalCoordinate corner ( int i ) const
    {
      return hostGeometry().local( hostGeometry().corner( i ) );
    }

    GlobalCoordinate global ( const LocalCoordinate &local ) const { return local; }

    LocalCoordinate local ( const GlobalCoordinate &global ) const { return global; }

    ctype integrationElement ( const LocalCoordinate &local ) const { return ctype( 1 ); }
    ctype volume () const { return hostReferenceElement().volume(); }

    GlobalCoordinate center () const { return hostReferenceElement().position( 0, 0 ); }

    const JacobianTransposed &jacobianTransposed ( const LocalCoordinate &local ) const
    {
      return PrismJacobianHelper< ctype, coorddimension, mydimension >::instance();
    }

    const JacobianInverseTransposed &jacobianInverseTransposed ( const LocalCoordinate &local ) const
    {
      return PrismJacobianHelper< ctype, mydimension, coorddimension >::instance();
    }

  private:
    const ReferenceElement< ctype, dimension > &hostReferenceElement () const
    {
      return hostReferenceElement_;
    }

    const HostGeometry &hostGeometry () const { return hostGeometry_; }

    HostGeometry hostGeometry_;
    typedef ReferenceElement< ctype, dimension > HostReferenceElement;
    const HostReferenceElement &hostReferenceElement_;
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_TANGENTIALGEOMETRY_HH
