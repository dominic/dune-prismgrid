#ifndef DUNE_GRID_PRISMGRID_ENTITYSEED_HH
#define DUNE_GRID_PRISMGRID_ENTITYSEED_HH

#include <dune/common/typetraits.hh>

namespace Dune
{

  // PrismEntitySeed
  // ---------------

  template< int codim, class GridImp >
  class PrismEntitySeed
  {
    typedef PrismEntitySeed< codim, GridImp > This;
    typedef typename remove_const< GridImp >::type::Traits Traits;

  public:
    typedef typename Traits::Grid Grid;

    static const int dimension = Traits::dimension;
    static const int codimension = codim;

  private:
    typedef typename Traits::template Codim< codimension >::EntityPointer EntityPointer;
    typedef typename EntityPointer::Entity Entity;
    typedef typename EntityPointer::Implementation EntityPointerImp; 
    typedef typename Entity::Implementation EntityImp; 

  public:
    typedef typename EntityImp::HostEntity::EntitySeed HostEntitySeed;

    PrismEntitySeed () {}

    PrismEntitySeed ( const HostEntitySeed &hostEntitySeed, const int subIndex )
    : hostEntitySeed_( hostEntitySeed ),
      subIndex_( subIndex )
    {}

    bool isValid () const
    {
      return hostEntitySeed_.isValid();
    }

    const HostEntitySeed &hostSeed () const { return hostEntitySeed_; }

    int subIndex () const { return subIndex_; }

  private:
    HostEntitySeed hostEntitySeed_;
    int subIndex_;
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_ENTITYSEED_HH
