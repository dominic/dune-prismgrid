#ifndef DUNE_GRID_PRISMGRID_COLUMNITERATOR_HH
#define DUNE_GRID_PRISMGRID_COLUMNITERATOR_HH

#include <dune/common/typetraits.hh>

#include <dune/grid/common/entityiterator.hh>

#include "entitypointer.hh"
#include "gridenums.hh"

namespace Dune
{

  // PrismColumnIterator
  // -------------------

  /** \ingroup ColumnIterator
   * 
   * \brief Additional iterator for column 
   * 
   * See the following code example for usage:
   * \code
   * typedef typename HostGrid::template Codim< 0 >::LeafIterator HostLeafIterator;
   * const HostLeafIterator end = hostGrid.template leafend< 0 >();
   * for( HostLeafIterator it = hostGrid.template leafbegin< 0 >(); it != end; ++it )
   * {
   *   const typename HostGrid::template Codim< 0 >::Entity &hostEntity = *it;
   *
   *   ColumnIteratorType direction = up;
   * 
   *   typedef typename Grid::template Codim< 0 >::ColumnIterator ColumnIterator;
   *   const ColumnIterator cend = grid.template cend< 0 >( hostEntity, direction );
   *   for( ColumnIterator cit = grid.template cbegin< 0 >( hostEntity, direction ); cit != cend; ++cit )
   *   {
   *     const typename Grid::template Codim< 0 >::Entity &entity = *cit;
   *   }
   * }
   * \endcode
   */
  template< int codim, class GridImp >
  class PrismColumnIterator
  : public PrismEntityPointer< PrismEntityPointerTraits< codim, GridImp > >
  {
    typedef PrismColumnIterator< codim, GridImp > This;
    typedef PrismEntityPointer< PrismEntityPointerTraits< codim, GridImp > > Base;

  public:
    //! \brief grid type
    typedef typename Base::Grid Grid;

    //! \brief entity pointer implementation type
    typedef This EntityPointerImp;
    //! \brief entity type
    typedef typename Base::Entity Entity;
    //! \brief entity implementation type
    typedef typename Base::EntityImp EntityImp;

    //! \brief host iterator type
    typedef typename Base::HostIterator HostIterator;
    //! \brief host entity type
    typedef typename HostIterator::Entity HostEntity;

  private:
    // we only support codimensions 0 and dim...
    static const bool isLateral = ( Base::codimension == 0 );

#ifndef DOXYGEN
  protected:
    using Base::entity_;
    using Base::hostIterator_;
    using Base::layers;
#endif // #ifndef DOXYGEN

  public:
    using Base::level;

    //! \brief constructor
    PrismColumnIterator ( const HostIterator &hostIterator, int subIndex,
                          const Grid &grid, ColumnIteratorType direction )
    : Base( hostIterator, subIndex, grid ),
      direction_( direction )
    {}

    //! \brief return begin iterator
    static PrismColumnIterator begin ( const HostEntity &hostEntity,
                                       const Grid &grid,
                                       ColumnIteratorType direction = up )
    {
      HostIterator hostIterator( hostEntity );

      if( direction == up )
        return This( hostIterator, 0, grid, direction );

      int subIndex = ( isLateral ) ? grid.layers() - 1 : grid.layers();
      return This( hostIterator, subIndex, grid, direction );
    }

    //! \brief return end iterator
    static PrismColumnIterator end ( const HostEntity &hostEntity,
                                     const Grid &grid,
                                     ColumnIteratorType direction = up )
    {
      HostIterator hostIterator( hostEntity );

      if( direction != up )
        return This( hostIterator, -1, grid, direction );

      int subIndex = ( isLateral ) ? grid.layers() : grid.layers() + 1;
      return This( hostIterator, subIndex, grid, direction );
    }

    //! \brief increment iterator
    void increment ()
    {
      int subIndex = Base::subIndex();
      if( direction_ == up )
        subIndex += 1;
      else
        subIndex -= 1;
      entity_.impl().reset( subIndex );
    }

  private:
    ColumnIteratorType direction_;;
  };

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_COLUMNITERATOR_HH
