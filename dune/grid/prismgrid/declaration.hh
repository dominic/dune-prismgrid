#ifndef DUNE_GRID_PRISMGRID_DECLARATION_HH
#define DUNE_GRID_PRISMGRID_DECLARATION_HH

namespace Dune
{

  template< class HostGrid >
  struct PrismGrid;

} // namespace Dune

#endif // #ifndef DUNE_GRID_PRISMGRID_DECLARATION_HH
