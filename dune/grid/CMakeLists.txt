add_subdirectory(prismgrid)
add_subdirectory(test)

set( HEADERS
  prismgrid.hh
)

install( FILES ${HEADERS} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/grid )
